package net.tepex.vtb;

import java.util.Date;

public class Item
{
	public String getId()
	{
		return id;
	}
	
	public String getSubject()
	{
		return subject;
	}
	
	public String getText()
	{
		return text;
	}
	
	public Date getStartDateTime()
	{
		return startDateTime;
	}
	
	public Date getEndDateTime()
	{
		return endDateTime;
	}
	
	@Override
	public int hashCode()
	{
		return id.hashCode();
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || !(obj instanceof Item)) return false;
		Item other = (Item)obj;
		return id.equals(other.id);
	}
	
	@Override
	public String toString()
	{
		return "[id: "+id+", subject: "+subject+", text: "+text+", start: "+startDateTime+", end: "+endDateTime+"]";
	}
	
	private String id;
	private String subject;
	private String text;
	private Date startDateTime;
	private Date endDateTime;
}
