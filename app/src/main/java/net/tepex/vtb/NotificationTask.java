package net.tepex.vtb;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;

import android.content.Context;
import android.content.Intent;

import android.os.Build;
import android.os.Handler;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import timber.log.Timber;

class NotificationTask extends TimerTask
{
	public NotificationTask(Context context)
	{
		this.context = context;
		notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
		/* Для версии 26 следует создать channelId */
	}

	@Override
	public void run()
	{
		Date now = new Date();
		for(Item item: getList())
		{
			Timber.d("check item: %s", item);
			if(!(now.before(item.getStartDateTime()) || now.after(item.getEndDateTime())))
			{
				Timber.d("    Create notification for: %s", item.getSubject());
				/* Про λ в ТЗ ничего не сказано :) */
				handler.post(() ->
				{
					Notification.Builder builder = new Notification.Builder(context)
						.setAutoCancel(true)
						.setSmallIcon(android.R.drawable.ic_dialog_info)
						.setContentTitle(item.getSubject())
						.setContentText(item.getText());

					Intent notifyIntent = new Intent(context, MainActivity.class);
					notifyIntent.putExtra(MainActivity.EXTRA_TITLE, item.getSubject());
					notifyIntent.putExtra(MainActivity.EXTRA_TEXT, item.getText());
					notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					builder.setContentIntent(
						PendingIntent.getActivity(context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT));

					notificationManager.notify(id++, builder.getNotification());
				});
			}
		}
	}

	@Override
	public boolean cancel()
	{
		context = null;
		notificationManager = null;
		return super.cancel();
	}

	private List<Item> getList()
	{
		String json;
		try
		{
			Request request = new Request.Builder()
				.url(URL)
				.build();
			Response response = httpClient.newCall(request).execute();

			json = response.body().string();
		}
		catch(IOException e)
		{
			Timber.e(e, "Service error!");
			return Collections.emptyList();
		}

		return gson.fromJson(json, new TypeToken<ArrayList<Item>>(){}.getType());
	}
		
	private Context context;
	private NotificationManager notificationManager;
	private Handler handler = new Handler();
	private int id = 1;
	
	private OkHttpClient httpClient = new OkHttpClient();
	private Gson gson = new Gson();
	
	private static final String URL = "https://bitbucket.org/Tepex/vtb-test/raw/9feab81c4afd15cd3f2119a8878e5577ed6b5a93/vtb.json";
}
