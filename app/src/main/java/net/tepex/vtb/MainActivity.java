package net.tepex.vtb;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;

import timber.log.Timber;

public class MainActivity extends Activity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Timber.d("create activity");
		startService(new Intent(this, MainService.class));
	}
	
	/* Bug workaround on Android M (v23)
	   https://stackoverflow.com/a/32169416	*/
	@Override
	protected void onStart()
	{
		super.onStart();
		setVisible(true);
		Intent intent = getIntent();
		String title = intent.getStringExtra(EXTRA_TITLE);
		String msg = intent.getStringExtra(EXTRA_TEXT);
		if(title == null) return;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this)
			.setTitle(title)
			.setMessage(msg)
			.setPositiveButton(android.R.string.ok, (dialog, which) ->
			{
				dialog.dismiss();
				finish();
			});
		builder.show();
	}
	
	public static final String EXTRA_TEXT = "text";
	public static final String EXTRA_TITLE = "title";
}
