package net.tepex.vtb;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import java.util.Timer;

import timber.log.Timber;

public class MainService extends Service
{
	@Override
	public void onCreate()
	{
		Timber.d("start service");
		if(timer != null) timer.cancel();
		else timer = new Timer();
		timer.scheduleAtFixedRate(new NotificationTask(getApplicationContext()), 0, INTERVAL);
	}
	
	@Override
	public IBinder onBind(Intent intent)
	{
		return null;
	}
	
	@Override
	public void onDestroy()
	{
		Timber.d("stop service");
		timer.cancel();
	}
	
	private Timer timer;
	private static final int INTERVAL = 2 * 60 * 1000;
}
